"use strict"

/*
 * Copyright (c) 2022 Samuel Sloniker
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

function format(number) {
    return (number < 10)?("0" + String(number)):String(number)
}

function adjust() {
    let ce = document.querySelector("#countdown") 
    let cec = document.querySelector("#countdownbox") 
    let cet = document.querySelector("#countdowntext") 

    let time = Math.ceil((newYear - new Date()) / 1000)
    if (time <= 0) {
        clearInterval(intervalID)
        cet.innerHTML = "HAPPY NEW YEAR"
        let fireworks = new Fireworks(document.querySelector("div#firepops"), { /* options */ })
        fireworks.start()
    } else {
        let days = Math.floor(time / (60 * 60 * 24))
        let hours = Math.floor(time / (60 * 60)) % 24
        let minutes = Math.floor(time / 60) % 60
        let seconds = Math.floor(time) % 60
        cet.textContent = (days?(format(days) + ":"):"") +
                          ((hours || days)?(format(hours) + ":"):"") +
                          ((minutes || hours || days)?(format(minutes) + ":"):"") +
                          format(seconds)
    }

    let fontSize = 1
    ce.style.fontSize = String(fontSize - 1) + "px"
    ce.style.fontSize = "1px"
    while (ce.clientHeight <= cec.clientHeight && cec.scrollWidth <= document.documentElement.clientWidth && fontSize <= 180) {
        fontSize += 1
        ce.style.fontSize = String(fontSize) + "px"
    }

}

function main() {
    window.newYear = new Date(new Date().getFullYear() + 1, 0, 1)
    document.querySelector("h1#header").textContent = "NEW YEAR " + String(newYear.getFullYear()) + " COUNTDOWN"
    window.intervalID = setInterval(adjust, 100)
    window.addEventListener("resize", adjust)
    adjust()
}

window.onload = main
